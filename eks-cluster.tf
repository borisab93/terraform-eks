provider "kubernetes" {
  load_config_file = "false"
  host = data.aws_eks_cluster.eks-cluster.endpoint
  token = data.aws_eks_cluster_auth.eks-cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks-cluster.certificate_authority.0.data)
}

data "aws_eks_cluster" "eks-cluster" {
  name = module.eks.cluster_name
}

data "aws_eks_cluster_auth" "eks-cluster" {
  name = module.eks.cluster_name
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "20.8.5"

  cluster_name = "eks-cluster"
  cluster_version = "1.28"
  cluster_endpoint_public_access = true

  subnet_ids = module.eks-vpc.private_subnets
  vpc_id = module.eks-vpc.vpc_id

  tags = {
    environment = "development"
    Tewrraform = "true"
  }

  eks_managed_node_group_defaults = {
    instance_types = ["t3.micro"]
  }

  eks_managed_node_groups = {
    example = {
      min_size     = 1
      max_size     = 2
      desired_size = 1

      instance_types = ["t3.micro"]
    }
  }
}